﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NuGet.DependencyResolver;
using NuGet.Protocol.Core.v3;
using Server.Database.Core;
using Server.Database.Entities;
using Server.Services.Interfaces;
using Server.WebSockets.Models;
using Server.WebSockets.Router.Modules;

namespace Server.Services {
    public class ContactService : IContactService {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConnectionModule _connectionModule;

        public ContactService(IUnitOfWork unitOfWork, IConnectionModule connectionModule) {
            _unitOfWork = unitOfWork;
            _connectionModule = connectionModule;
        }

        public async Task<List<ResponseModel>> AddContactInitalRequest(User user, string targetId) {
            var targetUser = _unitOfWork.UserRepository.GetUserByGuid(targetId);

            if (user.Contacts == null)
            {
                user.Contacts = new List<User>();
            }

            if(!user.Contacts.Contains(targetUser))
                user.Contacts.Add(targetUser);

            _unitOfWork.ContactRequestRepository.Add(new ContactRequest {
                Deleted = false,
                Initiator = user,
                Target = targetUser,
                TimeStamp = DateTime.Now
            });
            
            await _unitOfWork.Complete();

            return new List<ResponseModel> {
                new ResponseModel {
                    eventName = "CONTACT_ADD_REQUEST",
                    Data = new {
                        user = new {
                            userName = user.UserName,
                            id = user.Id
                        },
                    }.ToJToken(),
                    Users = new[] {targetId}
                }
            };
        }


        public async Task<List<ResponseModel>> AddContactAcceptanceRequest(User acceptor, string initiatorId) {
            var initiatorUser = await _unitOfWork.UserRepository.GetUserIncludingContacts(initiatorId);
            var contactRequest = _unitOfWork.ContactRequestRepository.GetContactRequestByUsers(initiatorUser, acceptor);

            if (acceptor.Contacts == null)
            {
                acceptor.Contacts = new List<User>();
            }

            if(!acceptor.Contacts.Contains(initiatorUser))
                acceptor.Contacts.Add(initiatorUser);

            foreach (var request in contactRequest)
            {
                request.Deleted = true;
            }

            await _unitOfWork.Complete();

            return new List<ResponseModel> {
                new ResponseModel {
                    eventName = "CONTACT_SUCCESSFULLY_ADDED",
                    Data = new {
                        contacts = await _unitOfWork.UserRepository
                            .GetUserContactList(acceptor.Id)
                            .ContinueWith(result => {
                                return result.Result.Select(x => new {
                                    username = x.UserName,
                                    id = x.Id,
                                    image = x.Image64,
                                    status = _connectionModule.ContainsUser(x.Id)
                                });
                            })
                    }.ToJToken(),
                    Users = new[] {acceptor.Id}
                },

                new ResponseModel {
                    eventName = "CONTACT_SUCCESSFULLY_ADDED",
                    Data = new {
                        contacts = await _unitOfWork.UserRepository
                            .GetUserContactList(initiatorId)
                            .ContinueWith(result => {
                                return result.Result.Select(x => new {
                                    username = x.UserName,
                                    id = x.Id,
                                    image = x.Image64,
                                    status = _connectionModule.ContainsUser(x.Id)
                                });
                            })
                    }.ToJToken(),
                    Users = new[] {initiatorId}
                }
            };
        }
    }
}