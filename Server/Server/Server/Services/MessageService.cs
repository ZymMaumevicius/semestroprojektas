﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Database.Core;
using Server.Database.Entities;
using Server.Services.Interfaces;
using Server.WebSockets.Extensions;
using Server.WebSockets.Models;

namespace Server.Services
{
    public class MessageService : IMessageService {
        private readonly IUnitOfWork _unitOfWork;

        public MessageService(IUnitOfWork unitOfWork) {
            _unitOfWork = unitOfWork;
        }

        public async Task<MessageRequestModel> GetMessages(string chatRoomId) {
            var chatRoom = await _unitOfWork.ChatRoomRepository.GetChatRoomByName(chatRoomId);

            var model = new MessageRequestModel {
                id = chatRoomId.ToArray(),
                messages = new MessageModel[0]
            };

            if (chatRoom != null) {
                model.messages = chatRoom.Messages
                    .OrderByDescending(x => x.CreatedAt)
                    .Take(100)
                    .Select(x => new MessageModel {
                        message = x.Content,
                        sender = x.Sender.Id,
                        date = x.CreatedAt.ToString()
                    })
                    .ToArray();
            }
            return model;
        }

        public async Task SaveMessage(MessageSaveModel model) {
            using (var trx = _unitOfWork.StartTransactionWithLock()) {
                var chatRoom = await _unitOfWork.ChatRoomRepository.GetChatRoomByName(model.id);
                if (chatRoom != null) {
                    chatRoom.Messages.Add(model.message);
                }
                else {
                    var newChatRoom = new ChatRoom {
                        Members = _unitOfWork.UserRepository.GetUsersListByIds(model.id.ToArray()),
                        Deleted = false,
                        Messages = new List<Message> {model.message},
                        Name = model.id
                    };

                    _unitOfWork.ChatRoomRepository.Add(newChatRoom);
                }

                await _unitOfWork.Complete();

                trx.Commit();
            }
        }
    }
}