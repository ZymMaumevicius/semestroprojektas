﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Server.Database.Entities;
using Server.WebSockets.Models;

namespace Server.Services.Interfaces {
    public interface IContactService {
        Task<List<ResponseModel>> AddContactInitalRequest(User user, string targetId);

        Task<List<ResponseModel>> AddContactAcceptanceRequest(User acceptor, string initiatorId);
    }
}