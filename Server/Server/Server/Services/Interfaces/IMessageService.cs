﻿using System.Threading.Tasks;
using Server.WebSockets.Models;

namespace Server.Services.Interfaces
{
    public interface IMessageService {
        Task<MessageRequestModel> GetMessages(string chatRoomId);
        Task SaveMessage(MessageSaveModel model);
    }
}
