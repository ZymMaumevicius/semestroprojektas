﻿using System.Threading.Tasks;

namespace Server.Services.Interfaces
{
    public interface IEmailSender
    {
        Task<bool> SendConfirmationEmail(string guid, string email);
    }
}