﻿using System;
using System.Text;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using Server.Configuration;
using Server.Services.Interfaces;

namespace Server.Services
{
    public class EmailSender : IEmailSender
    {
        private readonly EmailSettings _emailSettings;

        public EmailSender(IOptions<EmailSettings> emailSettings)
        {
            _emailSettings = emailSettings.Value;
        }

        public async Task<bool> SendConfirmationEmail(string guid, string email)
        {
            var hash = Convert.ToBase64String(Encoding.UTF8.GetBytes(guid));
            var message = new MimeMessage();

            message.From.Add(new MailboxAddress(_emailSettings.InboxName, _emailSettings.InboxEmail));
            message.To.Add(new MailboxAddress(email));
            message.Subject = "ISON chat registration confirmation";
            message.Body = new TextPart("plain")
            {
                Text = $"Please clickt this link to activate your account: http://localhost:5000/Registration/Confirm?user={hash}"
            };

            var client = new SmtpClient();

            await client.ConnectAsync(_emailSettings.SendServer, _emailSettings.SendPort);
            client.AuthenticationMechanisms.Remove("XOAUTH2");
            client.Authenticate(_emailSettings.Username, _emailSettings.Password);
            client.Send(message);
            client.Disconnect(true);

            return true;
        }
    }
}