﻿using System.Linq;
using NuGet.Protocol.Core.v3;
using Server.Database.Core;
using Server.WebSockets.Core;
using Server.WebSockets.Models;
using Server.WebSockets.Router;
using Server.WebSockets.Router.Modules;

namespace Server.WebSockets.Controllers
{
    public class SocketWebsocketController : WebsocketController {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConnectionModule _connectionModule;

        public SocketWebsocketController(ISender sender, IUnitOfWork unitOfWork, IConnectionModule connectionModule) : base(sender) {
            _unitOfWork = unitOfWork;
            _connectionModule = connectionModule;
        }

        public async void Connected(Request request) {
            var userId = request.User.ToObject<string[]>()[0];
            var contactList = await _unitOfWork.UserRepository.GetUserContactList(request.UserModel.Id);
            var contactRequests = _unitOfWork.ContactRequestRepository.GetContactRequestByUser(request.UserModel); 

            _sender.AddUser(userId, request.Websocket);
            _sender.SendMessage(new ResponseModel {
                eventName = "SOCKET_CONNECTED_RESULT",
                Data = contactList.Select(x => new {
                        username = x.UserName,
                        id = x.Id,
                        image = x.Image64,
                        status = _connectionModule.ContainsUser(x.Id)
                    })
                    .ToList()
                    .ToJToken(),
                Users = request.User.ToObject<string[]>()
            });

            foreach (var req in contactRequests)
            {
                _sender.SendMessage(new ResponseModel {
                    eventName = "CONTACT_ADD_REQUEST",
                    Data = new {
                        user = new {
                            userName = req.UserName,
                            id = req.Id
                        },
                    }.ToJToken(),
                    Users = request.User.ToObject<string[]>()
                });
            }

            foreach (var contact in contactList) {
                _sender.SendMessage(new ResponseModel {
                    eventName = "USER_CONNECTED",
                    Data = new {
                        id = userId
                    }.ToJToken(),
                    Users = new [] {
                        contact.Id
                    }
                });
            }
                
        }
    }
}