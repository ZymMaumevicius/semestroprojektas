﻿using System.Linq;
using NuGet.Protocol.Core.v3;
using Server.Database.Core;
using Server.Services.Interfaces;
using Server.WebSockets.Core;
using Server.WebSockets.Models;
using Server.WebSockets.Router;

namespace Server.WebSockets.Controllers
{
    public class ContactsWebsocketController : WebsocketController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IContactService _contactService;

        public ContactsWebsocketController(ISender sender, IUnitOfWork unitOfWork,
            IContactService contactService) : base(sender)
        {
            _unitOfWork = unitOfWork;
            _contactService = contactService;
        }

        public void SearchContact(Request request)
        {
            var term = request.RequestData["term"].ToString();
            var user = request.User.ToObject<string[]>()[0];
            var contacts = _unitOfWork.UserRepository.SearchContacts(term, user);

            _sender.SendMessage(new ResponseModel
            {
                Data = contacts.ToJToken(),
                eventName = "CONTACTS_SEARCH_RESULT",
                Users = request.User.ToObject<string[]>()
            });
        }

        public async void AddContact(Request request)
        {
            var userToAddGuid = request.RequestData["contactId"].ToString();

            var initialRequest = request.RequestData["initialRequest"].ToObject<bool>();

            var resposes = initialRequest
                ? await _contactService.AddContactInitalRequest(request.UserModel, userToAddGuid)
                : await _contactService.AddContactAcceptanceRequest(request.UserModel, userToAddGuid);

            foreach (var response in resposes)
            {
                _sender.SendMessage(response);
            }
        }

        public async void RemoveContact(Request request)
        {
            var userToRemoveGuid = request.RequestData["contactId"].ToString();
            var userWhoRemovesGuid = request.User.ToObject<string[]>()[0];

            var userToRemove = await _unitOfWork.UserRepository.GetUserIncludingContacts(userToRemoveGuid);
            var userWhoRemoves = await _unitOfWork.UserRepository.GetUserIncludingContacts(userWhoRemovesGuid);

            userWhoRemoves.Contacts.Remove(userToRemove);
            if(userToRemove.Contacts.Contains(userWhoRemoves))
                userToRemove.Contacts.Remove(userWhoRemoves);

            await _unitOfWork.Complete();

            _sender.SendMessage(new ResponseModel
            {
                eventName = "CONTACT_SUCCESSFULLY_ADDED",
                Data = new
                {
                    contacts = await _unitOfWork.UserRepository
                        .GetUserContactList(userWhoRemovesGuid)
                        .ContinueWith(result => {
                            return result.Result.Select(x => new {
                                username = x.UserName,
                                id = x.Id,
                                image = x.Image64
                            });
                        })
                }.ToJToken(),
                Users = new[] { userWhoRemovesGuid }
            });

            _sender.SendMessage(new ResponseModel
            {
                eventName = "CONTACT_SUCCESSFULLY_ADDED",
                Data = new
                {
                    contacts = await _unitOfWork.UserRepository
                        .GetUserContactList(userToRemoveGuid)
                        .ContinueWith(result => {
                            return result.Result.Select(x => new {
                                username = x.UserName,
                                id = x.Id,
                                image = x.Image64
                            });
                        })
                }.ToJToken(),
                Users = new[] { userToRemoveGuid }
            });
        }
    }
}