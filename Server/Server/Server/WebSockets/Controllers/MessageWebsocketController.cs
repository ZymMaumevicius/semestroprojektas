﻿using NuGet.Protocol.Core.v3;
using Server.Database.Core;
using Server.WebSockets.Core;
using Server.WebSockets.Models;
using Server.WebSockets.Router;
using System.Threading.Tasks;
using Server.Services.Interfaces;
using Server.WebSockets.Extensions;

namespace Server.WebSockets.Controllers
{
    public class MessageWebsocketController : WebsocketController {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMessageService _messageService;
        public MessageWebsocketController(IMessageService messageService, ISender sender, IUnitOfWork unitOfWork) : base(sender) {
            _unitOfWork = unitOfWork;
            _messageService = messageService;
        }

        public async void Send(Request request) {

            var users = request.RequestData["users"]["id"].ToObject<string[]>();
            
            _messageService
                .SaveMessage(request.ToModel())
                .ConfigureAwait(false);

            var response = new ResponseModel {
                Data = request.RequestData,
                Users = users,
                eventName = "MESSAGE_RECEIVED"
            };
            _sender.SendMessage(response);
        }

        public async Task Request(Request request) {

            var chatRoomIdsArray = request.RequestData["messageKey"].ToObject<string[]>();
            var chatRoomId = chatRoomIdsArray.CreateId();
            var model = await _messageService.GetMessages(chatRoomId);

            _sender.SendMessage(new ResponseModel {
                Data = model.ToJToken(),
                Users = request.User.ToObject<string[]>(),
                eventName = "MESSAGES_LOADED"
            });
        }

        public void LoadMore(Request request) {
            var chatRoomId = request.RequestData["chat"].ToObject<string>();
            var length = request.RequestData["length"].ToObject<int>();

            var messages = _unitOfWork.MessageRepository.GetOlderMessages(chatRoomId, length);

            _sender.SendMessage(new ResponseModel {
                Data = new {
                     chatRoomId,
                     messages
                    }.ToJToken(),
                Users = request.User.ToObject<string[]>(),
                eventName = "OLDER_MESSAGES_LOADED"
            });
        }
    }
}