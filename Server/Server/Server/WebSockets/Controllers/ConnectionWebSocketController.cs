﻿using System;
using System.Threading.Tasks;
using NuGet.Protocol.Core.v3;
using Server.Database.Core;
using Server.WebSockets.Core;
using Server.WebSockets.Models;
using Server.WebSockets.Router;
using Server.WebSockets.Router.Modules;

namespace Server.WebSockets.Controllers {
    public class ConnectionWebSocketController : WebsocketController {

        public ConnectionWebSocketController(ISender sender) : base(sender) {

        }

    }
}
