﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;
using NuGet.Protocol.Core.v3;
using Server.Database.Core;
using Server.WebSockets.Core;
using Server.WebSockets.Models;
using Server.WebSockets.Router;
using Server.WebSockets.Router.Modules;

namespace Server.WebSockets.Controllers
{
    public class DisconnectWebsocketController : WebsocketController {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConnectionModule _connectionModule;
        private readonly ISender sender;
        public DisconnectWebsocketController(ISender sender, IUnitOfWork unitOfWork, IConnectionModule module) : base(sender) {
            _unitOfWork = unitOfWork;
            _connectionModule = module;
            this.sender = sender;
        }

        public async void DisconnectUser(WebSocket socket) {

            var userId = _connectionModule.GetUserByWebsocket(socket);
            var contactList = await _unitOfWork.UserRepository.GetUserContactList(userId);

            Console.WriteLine(userId + " DCED");
            foreach (var contact in contactList)
            {
                _sender.SendMessage(new ResponseModel
                {
                    eventName = "USER_DISCONNECTED",
                    Data = new
                    {
                        id = userId
                    }.ToJToken(),
                    Users = new[] {
                        contact.Id
                    }
                });
            }
            await sender.RemoveUser(socket);
        }
    }
}
