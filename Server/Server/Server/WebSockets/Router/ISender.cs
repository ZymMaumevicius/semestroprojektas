﻿using System.Net.WebSockets;
using System.Threading.Tasks;
using Server.WebSockets.Models;

namespace Server.WebSockets.Router {
    public interface ISender {
        void SendMessage(ResponseModel responseModel);
        void AddUser(string name, WebSocket socket);
        Task RemoveUser(WebSocket socket);
    }
}