﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Server.Core;
using Server.WebSockets.Models;
using Server.WebSockets.Router.Modules;

namespace Server.WebSockets.Router {
    public class Sender : ISender {
        private readonly ConcurrentQueue<ResponseModel> _messageQueue;
        private readonly IConnectionModule _connectionModule;

        public Sender(IConnectionModule module) {
            _connectionModule = module;
            _messageQueue = new ConcurrentQueue<ResponseModel>();
            new ThreadWorker(100, StartWorker)
                .StartThreadWorker();
        }

        public void AddUser(string name, WebSocket socket) {
            _connectionModule.AddUser(name, socket);
        }

        public async Task RemoveUser(WebSocket socket) {
            _connectionModule.RemoveUser(socket);
        }

        public void SendMessage(ResponseModel responseModel) {
            _messageQueue.Enqueue(responseModel);
        }

        private async void StartWorker() {
            int bulkCount = 0;
            while (_messageQueue.Count != 0 && bulkCount < 20)
            {
                if (_messageQueue.Count == 0) continue;
                ResponseModel response;
                if (!_messageQueue.TryDequeue(out response)) continue;
                await SenderWorker(response);
                bulkCount++;
            }
        }

        private async Task SenderWorker(ResponseModel responseModel) {
            var response = JObject.FromObject(new {
                    eventName = responseModel.eventName,
                    data = responseModel.Data
                })
                .ToString();
            Console.WriteLine("Send message");
            foreach (var usr in responseModel.Users)
            {
                if (!_connectionModule.ContainsUser(usr)) continue;
                var sendAsync = _connectionModule.GetUser(usr)
                    ?.SendAsync(
                        new ArraySegment<byte>(Encoding.ASCII.GetBytes(response),
                            0,
                            response.Length),
                        WebSocketMessageType.Text,
                        true,
                        CancellationToken.None
                    );

                sendAsync?.ConfigureAwait(false);
            }
        }
    }
}