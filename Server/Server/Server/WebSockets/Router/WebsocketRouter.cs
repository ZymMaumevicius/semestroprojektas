﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Reflection;
using Newtonsoft.Json.Linq;
using Server.Database.Core;
using Server.WebSockets.Core;
using Server.WebSockets.Extensions;
using Server.WebSockets.Models;

namespace Server.WebSockets.Router {
    public class WebsocketRouter {
        private Dictionary<string, Type> _controllers;
        private IServiceProvider _context;

        public WebsocketRouter(IServiceProvider context) {
            _context = context;
            _controllers = new Dictionary<string, Type>();
            Resolve();
            Console.WriteLine("WebsocketRouter init");
        }

        private void Resolve() {
            foreach (var type in Assembly.GetEntryAssembly().ExportedTypes) {
                if (type.GetTypeInfo().BaseType != typeof(WebsocketController)) continue;
                var name = type.Name.Replace("WebsocketController", "");
                _controllers.Add(name, type);
            }
        }

        public string Route(Request request) {
            try {
                IUnitOfWork uof = (IUnitOfWork) _context.GetService(typeof(IUnitOfWork));
                request.UserModel = uof.UserRepository.GetUserIncludingContacts(request.User.ToObject<string[]>()[0]).Result;


                var eventName = request.Type.ToString();

                var splitedEventName = eventName.Split('_');
                splitedEventName[0] = splitedEventName[0].ToCamelCase();
                splitedEventName[1] = splitedEventName[1].ToCamelCase();

                var instance = _context.GetService(_controllers[splitedEventName[0]]);

                var method = instance
                    .GetType()
                    .GetMethod(splitedEventName[1]);

                var result = method.Invoke(instance, new[] {request});

                return "";
            }
            catch (Exception e) {
                Console.WriteLine(e.StackTrace);
                return JObject.FromObject(
                        new {
                            eventName = "ERROR",
                            data = new {
                                message = "Route does not exist",
                                emessage = e.Message,
                                stackTrace = e.StackTrace
                            }
                        })
                    .ToString();
            }
        }

        public void RouteToDisconnect(WebSocket socket) {
            var a = _context.GetService(_controllers["Disconnect"]);
            var method = a
                   .GetType()
                   .GetMethod("DisconnectUser");
            method.Invoke(a, new[] {socket});
        }
    }
}