﻿using System;
using System.Collections.Concurrent;
using System.Net.WebSockets;
using System.Threading.Tasks;
using MimeKit.Cryptography;

namespace Server.WebSockets.Router.Modules {
    public class ConnectionModule : IConnectionModule {
        private readonly ConcurrentDictionary<string, WebSocket> _users;


        public ConnectionModule() {
            _users = new ConcurrentDictionary<string, WebSocket>();
        }

        public void AddUser(string id, WebSocket socket) {
            _users.AddOrUpdate(id, socket, (key, oldSocket) => socket);
        }

        public void RemoveUser(WebSocket socket) {
            foreach (var keyValuePair in _users) {
                if (!keyValuePair.Value.Equals(socket)) continue;
                WebSocket sckt;
                if (_users.TryRemove(keyValuePair.Key, out sckt)) {
                    Console.WriteLine("Users left: " + _users.Count);
                }
                return;
            }
        }

        public WebSocket GetUser(string id) {
            return _users[id];
        }

        public bool ContainsUser(string id) {
            return _users.ContainsKey(id);
        }

        public string GetUserByWebsocket(WebSocket socket) {
            foreach (var keyValuePair in _users) {
                if (keyValuePair.Value.Equals(socket))
                    return keyValuePair.Key;
            }
            return String.Empty;
        }
    }
}