﻿using System.Net.WebSockets;

namespace Server.WebSockets.Router.Modules {
    public interface IConnectionModule {
        void AddUser(string id, WebSocket socket);
        void RemoveUser(WebSocket socket);
        WebSocket GetUser(string id);
        bool ContainsUser(string id);
        string GetUserByWebsocket(WebSocket socket);
    }
}