﻿using Server.Database.Entities;

namespace Server.WebSockets.Models
{
    public class MessageSaveModel
    {
        public string id { get; set; }
        public Message message { get; set; }
    }
}
