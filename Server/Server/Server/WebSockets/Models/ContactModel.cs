﻿namespace Server.WebSockets.Models
{
    public class ContactModel
    {
        public string id { get; set; }
        public string username { get; set; }
        public string image { get; set; }
    }
}