﻿using System.Net.WebSockets;
using Newtonsoft.Json.Linq;
using Server.Database.Entities;

namespace Server.WebSockets.Models {
    public class Request {
        public WebSocket Websocket { get; set; }
        public JToken RequestData { get; set; }
        public JToken User { get; set; }
        public JToken Type { get; set; }
        public User UserModel { get; set; }
    }
}