﻿using Newtonsoft.Json.Linq;

namespace Server.WebSockets.Models {
    public class ResponseModel {
        public string eventName { get; set; }
        public string[] Users { get; set; }
        public JToken Data { get; set; }
    }
}