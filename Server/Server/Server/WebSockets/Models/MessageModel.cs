﻿using System;
using Server.Database.Entities;

namespace Server.WebSockets.Models
{
    public class MessageModel {
        public string sender { get; set; }
        public string date { get; set; }
        public string message { get; set; }
    }
}
