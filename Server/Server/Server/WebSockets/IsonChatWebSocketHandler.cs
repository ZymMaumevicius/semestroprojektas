﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyModel;
using Newtonsoft.Json.Linq;
using Server.WebSockets.Core;
using Server.WebSockets.Models;
using Server.WebSockets.Router;

namespace Server.WebSockets {
    public class IsonChatWebSocketHandler : WebSocketHandler {
        public IsonChatWebSocketHandler(WebSocketConnectionManager webSocketConnectionManager,
            ISender sender, WebsocketRouter router) : base(webSocketConnectionManager) {
            Sender = sender;
            _router = router;
        }

        private int err = 0;
        private int msge = 0;
        private ISender Sender { get; }
        private WebsocketRouter _router { get; }

        public override async Task OnConnected(WebSocket socket) {
            await base.OnConnected(socket);
            // foreach (var sckt in WebSocketConnectionManager.GetAll())
            // {
            //     Console.WriteLine(sckt.Key);
            // }

            // // Todo: send event when connected.
            // var socketId = WebSocketConnectionManager.GetId(socket);
            // await SendMessageToAllAsync("{\"socketId\":\"" + socketId + "\"}");
        }

        public override async Task OnDisconnected(WebSocket socket) {
            _router.RouteToDisconnect(socket);
            await base.OnDisconnected(socket);     
        }

        public override async Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer) {
            var msg = Encoding.UTF8.GetString(buffer, 0, buffer.Length);

            try
            {
                msge++;
                var data = JObject.Parse(msg);
                Console.WriteLine(err + " : " + msge);
                _router.Route(
                    new Request {
                        Websocket = socket,
                        RequestData = data["data"],
                        User = data["user"],
                        Type = data["eventName"]
                    });
            }
            catch (Exception e)
            {
                err++;
                Console.WriteLine(e.Message);
            }
        }
    }
}