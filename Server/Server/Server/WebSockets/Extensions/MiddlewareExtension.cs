﻿using System;
using System.Net.WebSockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Server.WebSockets.Core;
using Server.WebSockets.Router;

namespace Server.WebSockets.Extensions
{
    public static class MiddlewareExtension
    {
        public static IApplicationBuilder MapWebSocketManager(this IApplicationBuilder app, PathString path,
            WebSocketHandler handler)
        {
            return app.Map(path, builder => builder.UseMiddleware<WebSocketManagerMiddleware>(handler));
        }

        public static IServiceCollection AddWebSocketManger(this IServiceCollection services)
        {
            services.AddSingleton<ISender, Sender>();
            services.AddTransient<WebSocketConnectionManager>();
            services.AddTransient<WebsocketRouter>();

            foreach (var type in Assembly.GetEntryAssembly().ExportedTypes)
            {
                if (type.GetTypeInfo().BaseType == typeof(WebSocketHandler))
                {
                    services.AddSingleton(type);
                }

                if (type.GetTypeInfo().BaseType == typeof(WebsocketController))
                {
                    services.AddTransient(type);
                }
            }

            return services;
        }
    }
}