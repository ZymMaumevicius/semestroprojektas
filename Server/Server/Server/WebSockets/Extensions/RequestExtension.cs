﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Database.Entities;
using Server.WebSockets.Models;

namespace Server.WebSockets.Extensions {
    public static class RequestExtension {
        public static MessageSaveModel ToModel(this Request request) {
            var users = request.RequestData["users"]["id"].ToObject<string[]>().CreateId();
            var message = new Message {
                Content = request.RequestData["message"]["message"].ToObject<string>(),
                CreatedAt = request.RequestData["date"].ToObject<DateTime>(),
                Sender = request.UserModel
            };

            return new MessageSaveModel {
                id = users,
                message = message
            };
        }
    }
}