﻿using System;
using System.Linq;

namespace Server.WebSockets.Extensions {
    static class StringExtension {
        public static string ToCamelCase(this string text) {
            var split = text.Split('-');

            return split.Aggregate(String.Empty,
                (current, item) => current + item.Substring(0, 1).ToUpper() + item.Substring(1).ToLower());
        }

        public static string CreateId(this string[] array) {
            var id = String.Join(",", array.OrderBy(x => x));
            return id;
        }
        public static string[] ToArray(this string text) {
            var id = text.Split(',');
            return id;
        }

    }
}