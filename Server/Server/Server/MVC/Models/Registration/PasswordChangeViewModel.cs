﻿namespace Server.MVC.Models.Registration {
    public class PasswordChangeViewModel {
        public string oldPassword { get; set; }
        public string newPassword { get; set; }
        public string id { get; set; }
    }
}