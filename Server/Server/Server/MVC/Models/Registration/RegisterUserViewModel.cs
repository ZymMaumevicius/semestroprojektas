﻿namespace Server.MVC.Models.Registration
{
    public class RegisterUserViewModel
    {
        public string userName { get; set; }
        public string password { get; set; }
        public string email { get; set; }
    }
}