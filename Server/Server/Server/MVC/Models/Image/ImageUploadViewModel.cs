﻿namespace Server.MVC.Models.Image {
    public class ImageUploadViewModel {
        public string image { get; set; }
        public string id { get; set; }
    }
}