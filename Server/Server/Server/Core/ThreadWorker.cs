﻿using System;
using System.Threading;

namespace Server.Core {
    public class ThreadWorker {
        private readonly Thread _thread;
        private Timer _timer;
        public int Timeout { get; }
        private readonly Action _action;

        public ThreadWorker(int seconds, Action action) {
            Timeout = seconds;
            _thread = new Thread(StartThread) {IsBackground = true};
            _action = action;
        }

        public void StartThreadWorker() {
            _thread.Start();
        }

        private void StartThread() {
            _timer = new Timer(TimerWork, null,Timeout, System.Threading.Timeout.Infinite);
        }

        private void TimerWork(object state) {
            lock (_timer)
            {
                _action.Invoke();
                _timer.Change(Timeout, System.Threading.Timeout.Infinite);
            }

        }
    }
}