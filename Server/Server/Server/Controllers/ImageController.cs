﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Server.Database.Core;
using Server.MVC.Models.Image;

namespace Server.Controllers {
    public class ImageController : Controller {

        private readonly IUnitOfWork _unitOfWork;

        public ImageController(IUnitOfWork unitOfWork) {
            _unitOfWork = unitOfWork;
        }

        [HttpPost]
        public async Task<IActionResult> Index([FromBody] ImageUploadViewModel imageUploadViewModel) {
            var user = _unitOfWork.UserRepository.GetUserByGuid(imageUploadViewModel.id);
            user.Image64 = imageUploadViewModel.image;
            await _unitOfWork.Complete();

            return Ok(new {
                image = user.Image64 ?? ""
            });
        }
    }
}