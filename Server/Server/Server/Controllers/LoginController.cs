﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Server.Database.Core;
using Server.Database.Entities;
using Server.MVC.Models.Login;

namespace Server.Controllers {
    public class LoginController : Controller {
        private readonly SignInManager<User> _signInManager;
        private readonly IUnitOfWork _unitOfWork;

        public LoginController(SignInManager<User> signInManager, IUnitOfWork unitOfWork) {
            _signInManager = signInManager;
            _unitOfWork = unitOfWork;
        }

        [HttpPost]
        [Route("/login")]
        public async Task<IActionResult> Index([FromBody] UserLoginViewModel model) {
            var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, true,
                false);

            if (!result.Succeeded)
                return BadRequest("Password incorrect");

            var UserInfo = await _unitOfWork.UserRepository.GetUserByUserName(model.UserName);
            
            if(!UserInfo.EmailConfirmed) {
                return BadRequest();
            } 

            return Ok(new {
                id = UserInfo.Id,
                username = UserInfo.UserName,                
                email = UserInfo.Email,
                image = UserInfo.Image64 ?? ""
            });
        }

        [HttpPost]
        [Route("/logout")]
        public async Task<IActionResult> LogOut() {
            await _signInManager.SignOutAsync();
            return Ok();
        }
    }
}