﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Server.Database.Core;
using Server.Database.Entities;
using Server.MVC.Models.Registration;
using Server.Services.Interfaces;

namespace Server.Controllers {
    public class RegistrationController : Controller {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEmailSender _emailSender;
        private readonly UserManager<User> _userManager;

        public RegistrationController(IUnitOfWork unitOfWork, IEmailSender emailSender, UserManager<User> userManager) {
            _unitOfWork = unitOfWork;
            _emailSender = emailSender;
            _userManager = userManager;
        }

        [HttpPost]
        public async Task<IActionResult> Index([FromBody] RegisterUserViewModel model) {
            var record = new User {
                UserName = model.userName,
                Email = model.email,
                Activated = false
            };
            var result = await _userManager.CreateAsync(record, model.password);

            if (result.Errors.Any())
            {
                return BadRequest(result.Errors);
            }

            var user = _unitOfWork.UserRepository.Find(e => e.Email.Equals(model.email)).FirstOrDefault();

            if (user == null) return BadRequest("User with given email does not exist.");

            await _emailSender.SendConfirmationEmail(user.Id, user.Email);

            return Ok();
        }

        public async Task<IActionResult> Confirm(string user) {
            var guid = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(user));
            var userEntity = _unitOfWork.UserRepository.GetUserByGuid(guid);

            if (userEntity == null) return BadRequest("User was not found.");

            userEntity.EmailConfirmed = true;
            await _unitOfWork.Complete();
            return View("~/MVC/Views/Confirm.cshtml");
        }

        public async Task<IActionResult> TestMail() {
            var result = await _emailSender.SendConfirmationEmail("ldjfghkdfghsdkshldgsdg", "pricindalas@gmail.com");
            if (result)
                return Ok();

            return BadRequest("Nepaejo");
        }

        [HttpPost]
        [Route("/change-password")]
        public async Task<IActionResult> ChangePassword([FromBody] PasswordChangeViewModel passwordChange) {
            var user = _unitOfWork.UserRepository.GetUserByGuid(passwordChange.id);

            if (!await _userManager.CheckPasswordAsync(user, passwordChange.oldPassword)) return BadRequest();

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var result = await _userManager.ResetPasswordAsync(user, token, passwordChange.newPassword);

            return Ok(result);
        }
    }
}