﻿using Microsoft.AspNetCore.Mvc;
using Server.Services.Interfaces;

namespace Server.Controllers
{
    public class MainController: Controller
    {
        public IActionResult Index()
        {
            return Ok();
        }
    }
}