﻿namespace Server.Configuration
{
    public class EmailSettings
    {
        public string InboxEmail { get; set; }
        public string InboxName { get; set; }
        public string SendServer { get; set; }
        public int SendPort { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}