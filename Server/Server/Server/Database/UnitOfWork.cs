﻿using System.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Server.Database.Core;

namespace Server.Database {
    public class UnitOfWork : IUnitOfWork {
        private readonly IsonContext _context;

        public IChatRoomRepository ChatRoomRepository { get; }
        public IMessageRepository MessageRepository { get; }
        public IUserRepository UserRepository { get; }
        public IContactRequestRepository ContactRequestRepository { get; }

        public UnitOfWork(IsonContext context,
            IChatRoomRepository chatRoomRepository,
            IMessageRepository messageRepository,
            IUserRepository userRepository,
            IContactRequestRepository contactRequestRepository
        ) {
            _context = context;
            ChatRoomRepository = chatRoomRepository;
            MessageRepository = messageRepository;
            UserRepository = userRepository;
            ContactRequestRepository = contactRequestRepository;
        }

        public Task<int> Complete() {
            return _context.SaveChangesAsync();
        }

        public void Dispose() {
            _context.Dispose();
        }

        public IDbContextTransaction  StartTransactionWithLock() {
            return _context.Database.BeginTransaction(IsolationLevel.Serializable);
        }
    }
}