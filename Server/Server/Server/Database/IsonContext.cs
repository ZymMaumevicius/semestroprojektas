﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Server.Database.Entities;

namespace Server.Database
{
    public class IsonContext : IdentityDbContext<User>
    {
        public DbSet<ChatRoom> ChatRooms { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<ContactRequest> ContactRequests { get; set; }

        public IsonContext(DbContextOptions<IsonContext> options) : base(options)
        {
        }
    }
}