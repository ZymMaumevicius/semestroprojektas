﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Server.Database.Entities
{
    public class Message
    {
        [Key]
        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool Deleted { get; set; }

        public ChatRoom ChatRoom { get; set; }
        public User Sender { get; set; }
    }
}