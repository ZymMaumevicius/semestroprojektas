﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Server.Database.Entities {
    public class ContactRequest {

        [Key]
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public User Initiator { get; set; }
        public User Target { get; set; }
        public bool Deleted { get; set; }
    }
}