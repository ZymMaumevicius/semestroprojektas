﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Server.Database.Entities
{
    public class ChatRoom
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Deleted { get; set; }

        public List<Message> Messages { get; set; }
        public List<User> Members { get; set; }
    }
}