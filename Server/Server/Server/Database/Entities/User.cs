﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Server.Database.Entities
{
    public class User : IdentityUser
    {
        public bool Activated { get; set; }
        public string Image64 { get; set; }
        // Contacts
        public List<User> Contacts { get; set; }

    }
}