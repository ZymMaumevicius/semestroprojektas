﻿using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Server.Database.Entities;

namespace Server.Database.Core
{
    public interface IChatRoomRepository : IRepository<ChatRoom> {
       Task<ChatRoom> GetChatRoomByName(string name);
    }
}