﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Server.Database.Entities;
using Server.WebSockets.Models;

namespace Server.Database.Core
{
    public interface IUserRepository : IRepository<User>
    {
        User GetUserByGuid(string guid);

        Task<User> GetUserByUserName(string userName);

        Task<List<User>> GetUserContactList(string id);

        IEnumerable<ContactModel> SearchContacts(string term, string userId);

        Task<User> GetUserIncludingContacts(string id);

        List<User> GetUsersListByIds(string[] ids);
    }
}