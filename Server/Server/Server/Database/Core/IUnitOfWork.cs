﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;

namespace Server.Database.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IChatRoomRepository ChatRoomRepository { get; }
        IMessageRepository MessageRepository { get; }
        IUserRepository UserRepository { get; }
        IContactRequestRepository ContactRequestRepository { get; }
        Task<int> Complete();

        IDbContextTransaction StartTransactionWithLock();
    }
}