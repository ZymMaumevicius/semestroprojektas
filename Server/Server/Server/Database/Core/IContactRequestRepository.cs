﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Server.Database.Entities;

namespace Server.Database.Core {
    public interface IContactRequestRepository : IRepository<ContactRequest> {
        IEnumerable<ContactRequest> GetContactRequestByUsers(User iniciator, User target);
        IEnumerable<User> GetContactRequestByUser(User target);
    }
}