﻿using System.Collections.Generic;
using Server.Database.Entities;
using Server.WebSockets.Models;

namespace Server.Database.Core
{
    public interface IMessageRepository : IRepository<Message> {
        List<MessageModel> GetOlderMessages(string chatRoomId, int currentLength);
    }
}