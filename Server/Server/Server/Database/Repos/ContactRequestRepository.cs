﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Server.Database.Core;
using Server.Database.Entities;

namespace Server.Database.Repos {
    public class ContactRequestRepository : Repository<ContactRequest>, IContactRequestRepository {
        public ContactRequestRepository(IsonContext context) : base(context) { }

        public IEnumerable<ContactRequest> GetContactRequestByUsers(User iniciator, User target) {
            return IsonContext.ContactRequests.Where(
                    x => x.Initiator == iniciator
                         && x.Target == target
                         && x.Deleted == false
                )
                .ToList();
        }

        public IEnumerable<User> GetContactRequestByUser(User target) {
            return IsonContext.ContactRequests.Where(
                    x => x.Target.Id == target.Id
                         && !x.Deleted
                )
                .Select(x => x.Initiator)
                .Distinct()
                .ToList();
        }


        public IsonContext IsonContext => Context as IsonContext;
    }
}