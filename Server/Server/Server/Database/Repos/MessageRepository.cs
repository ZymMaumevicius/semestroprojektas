﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Server.Database.Core;
using Server.Database.Entities;
using Server.WebSockets.Models;

namespace Server.Database.Repos {
    public class MessageRepository : Repository<Message>, IMessageRepository {
        public MessageRepository(IsonContext context) : base(context) { }

        public List<MessageModel> GetOlderMessages(string chatRoomId, int currentLength) {
            return Context.Messages
                .OrderByDescending(x => x.CreatedAt)
                .Skip(currentLength)
                .Take(20)
                .Select(x => new MessageModel {
                    sender = x.Sender.Id,
                    date = x.CreatedAt.ToString(),
                    message = x.Content
                })
                .ToList();
        }
    }
}