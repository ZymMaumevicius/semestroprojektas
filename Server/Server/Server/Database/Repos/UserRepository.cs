﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Org.BouncyCastle.Asn1.X9;
using Remotion.Linq.Clauses;
using Server.Database.Core;
using Server.Database.Entities;
using Server.WebSockets.Models;

namespace Server.Database.Repos {
    public class UserRepository : Repository<User>, IUserRepository {
        public UserRepository(IsonContext context) : base(context) { }

        public User GetUserByGuid(string guid) {
            return IsonContext.Find<User>(guid);
        }

        public Task<User> GetUserIncludingContacts(string id) {
            return IsonContext.Users.Include(x => x.Contacts).SingleOrDefaultAsync(x => x.Id == id);
        }

        public Task<User> GetUserByUserName(string userName) {
            return IsonContext.Users.SingleOrDefaultAsync(x => x.UserName == userName);
        }

        public async Task<List<User>> GetUserContactList(string id) {
            var userEntity = await IsonContext.Users.Include(x => x.Contacts).SingleOrDefaultAsync(x => x.Id == id);
            var contacts = (
                from user in IsonContext.Users
                join contactRequest in IsonContext.ContactRequests
                on user.Id equals contactRequest.Initiator.Id
                where contactRequest.Deleted && user.Id == id
                select contactRequest.Target.Id
            ).ToList();

            var contacts2 = (
                from user in IsonContext.Users
                join contactRequest in IsonContext.ContactRequests
                on user.Id equals contactRequest.Target.Id
                where contactRequest.Deleted && user.Id == id
                select contactRequest.Initiator.Id
            ).ToList();

            return userEntity.Contacts.Where(x => contacts.Contains(x.Id) || contacts2.Contains(x.Id)).ToList() ??
                   new List<User>();
        }

        public IEnumerable<ContactModel> SearchContacts(string term, string userId) {
            var result = IsonContext.Users
                .Where(e =>
                    e.EmailConfirmed
                    && e.UserName.Contains(term)
                    && e.Id != userId
                    && !IsonContext.Users.Single(x => x.Id == userId).Contacts.Contains(e))
                .Select(e => new ContactModel {
                    username = e.UserName,
                    id = e.Id,
                    image = e.Image64
                })
                .ToList();

            return result;
        }

        public List<User> GetUsersListByIds(string[] ids) {
            return IsonContext.Users.Where(x => ids.Contains(x.Id)).ToList();
        }

        public IsonContext IsonContext => Context as IsonContext;
    }
}