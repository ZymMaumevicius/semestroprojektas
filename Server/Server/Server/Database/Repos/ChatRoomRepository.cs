﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Server.Database.Core;
using Server.Database.Entities;

namespace Server.Database.Repos
{
    public class ChatRoomRepository : Repository<ChatRoom>, IChatRoomRepository
    {
        public ChatRoomRepository(IsonContext context) : base(context)
        {

        }
        public IsonContext IsonContext => Context as IsonContext;

        public Task<ChatRoom> GetChatRoomByName(string name)
        {
            return IsonContext.ChatRooms.Include(x => x.Messages).SingleOrDefaultAsync(x => x.Name == name);
        }
    }
}