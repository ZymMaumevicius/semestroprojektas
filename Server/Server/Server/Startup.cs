﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Server.Configuration;
using Server.Database;
using Server.Database.Core;
using Server.Database.Entities;
using Server.Database.Repos;
using Server.Services;
using Server.Services.Interfaces;
using Server.WebSockets;
using Server.WebSockets.Extensions;
using Server.WebSockets.Router.Modules;

namespace Server
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; set; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("project.json", true);

            Configuration = builder.Build();


        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddWebSocketManger();
            services
                .AddIdentity<User, IdentityRole>(options =>
                {
                    options.Password.RequireDigit = true;
                    options.Password.RequireLowercase = true;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = true;
                    options.Password.RequiredLength = 8;
                })
                .AddEntityFrameworkStores<IsonContext>()
                .AddDefaultTokenProviders();

            // Configuration
            services.AddOptions();
            services.Configure<DatabaseSettings>(settings =>
            {
                settings.ConnectionString = Configuration.GetSection("DatabaseSettings")["ConnectionString"];
            });
            services.Configure<EmailSettings>(settings =>
            {
                var section = Configuration.GetSection("EmailSettings");
                settings.InboxEmail = section["InboxEmail"];
                settings.InboxName = section["InboxName"];
                settings.Password = section["Password"];
                settings.SendPort = Convert.ToInt32(section["SendPort"]);
                settings.SendServer = section["SendServer"];
                settings.Username = section["Username"];
            });

            var connectionString = Configuration.GetSection("DbContextSettings")["ConnectionString"];

            services.AddDbContext<IsonContext>(op => op.UseNpgsql(connectionString));

            #region Dependency injection

            // Database         
            services.AddTransient<IChatRoomRepository, ChatRoomRepository>();
            services.AddTransient<IMessageRepository, MessageRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IContactRequestRepository, ContactRequestRepository>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddSingleton<IConnectionModule, ConnectionModule>();

            // Services
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<IMessageService, MessageService>();
            services.AddTransient<IContactService, ContactService>();
            services.AddTransient<IConnectionService, ConnectionService>();
            ;
            #endregion

            #region Settings

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;

                //User settings
                options.User.RequireUniqueEmail = true;
                #endregion
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IServiceProvider serviceProvider)
        {
            app.UseDeveloperExceptionPage();
            app.UseStaticFiles();
            app.UseWebSockets();
            app.UseIdentity();
            app.MapWebSocketManager("/ws", serviceProvider.GetService<IsonChatWebSocketHandler>());

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "{controller}/{action}/{id?}",
                    new {controller = "Main", action = "Index"});
            });
        }
    }
}
