import _ from 'lodash';
import { app, BrowserWindow } from 'electron';
import path from 'path';
import url from 'url';
import { ipcMain } from 'electron';
import notification from './src/backend/Notifications';
import settings from 'electron-settings';

import localStorage from './src/core/localStorage';
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;

function createWindow() {
	if (!settings.has('userSettings')) {
		settings.set('userSettings', {
			soundEnabled: true,
			theme: 'dark'
		});
	}


	// Create the browser window.
	win = new BrowserWindow({ width: 1100, height: 620, frame: false });

	// and load the index.html of the app.
	win.loadURL(url.format({
		pathname: path.join(__dirname, 'src/frontend/index.html'),
		protocol: 'file:',
		slashes: true
	}));

	win.setMinimumSize(600, 500);
	win.setMenu(null);
	// Open the DevTools.
	win.webContents.openDevTools();

	const store = new localStorage({
		configName: 'config',
		defaults: {
			name: '123'
		}
	});

	ipcMain.on('LOGIN', (event, messages) => {
		_.forEach(messages, (val, key) => {
			store.set(key, val);
		});
	});

	ipcMain.on('NOTIFICATION',(event, {message})=> {
		if (!win.isFocused()) {
			notification.notifyPopUp(message);
		}
	});

	ipcMain.on('EXIT', () => {
		win.close();
	});

	ipcMain.on('CHANGE_SIZE', () => {
		if (win.isMaximized()) {
			win.unmaximize();
		} else {
			win.maximize();
		}
	});

	ipcMain.on('MINIMIZE', () => {
		win.minimize();
	});

	// Emitted when the window is closed.
	win.on('closed', () => {
		// Dereference the window object, usually you would store windows
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		win = null;
	});
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
	// On macOS it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	// On macOS it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (win === null) {
		createWindow();
	}
});

app.on('window-resize', (e) => {
	e.preventDefault();
});
// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
