import notification from 'node-notifier';
import path from 'path';

export default {
	notifyPopUp(message) {
		notification.notify({
			title: message.name,
			message: message.message,
			icon: path.join(__dirname,'..','public','images', 'small-logo.png')
		});
	}
}