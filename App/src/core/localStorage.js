import electron from 'electron';
import path from 'path';
import fs from 'fs';

export default class LocalStorage {
	constructor(args) {
		const userDataPath = (electron.app || electron.remote.app).getPath('userData');

		this.path = path.join(userDataPath, args.configName + '.json');

		this.data = this._parseDataFile(this.path, args.defaults);
	}

	get(key) {
		return this.data[key];
	}

	set(key, val) {
		this.data[key] = val;

		fs.writeFileSync(this.path, this.data);
	}

	_parseDataFile(filePath, defaults) {
		try {
			return fs.readFileSync(filePath);
		} catch (error) {
			return defaults;
		}
	}
}