import _ from 'lodash';
import localStorage from './localStorage';

export default class WebSockeWrapper {
	constructor(url, user, protocols = []) {
		this.url = url;
		this.protocols = protocols;
		this.events = {};
		this.storage = new localStorage({
			configName: 'config',
			defaults: {
				email: '123'
			}
		});
		this.user = user;

		this.socket = new WebSocket(this.url + '/ws');

		this.socket.onopen = () => {
			this.send('SOCKET_CONNECTED',user.id);
		};

		this.socket.onmessage = (event) => {
			this._fireEvent(JSON.parse(event.data));
		};
	}

	close() {
		this.socket.close();
	}

	on(eventName, func) {
		if (!this.events[eventName]) {
			this.events[eventName] = [];
		}
		this.events[eventName].push(func);
	}

	send(eventName, data) {
		const obj = {
			eventName : eventName,
			data: data,
			user: [this.user.id]
		};
		_.isEmpty(obj.data) && delete obj.data;

		this.socket.send(JSON.stringify(obj));
	}

	getUser() {
		return this.user;
	}

	_fireEvent(event) {
		_.forEach(this.events[event.eventName], (callback) => {
			callback(event.data);
		});
	}
}