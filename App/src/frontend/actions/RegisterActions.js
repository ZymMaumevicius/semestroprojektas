import http from '../../core/http';
import Notifications from 'react-notification-system-redux';

export default {
	register
};

function register(payload) {
	return dispatch => {
		return http.post('/registration', payload)
			.then(() => {
				dispatch({
					type: '@@router/LOCATION_CHANGE',
					payload: {
						action: 'POP',
						hash: '',
						key: null,
						pathname: '/',
						search: ''
					}
				});
			})
			.catch((err) => {
				const notificationOtps = {
					// uid: 'once-please', // you can specify your own uid if required
					title: 'An error has occurred',
					message: err.toString(),
					position: 'br',
					autoDismiss: 10
				};
				dispatch(Notifications.error(notificationOtps));
				//TODO: error handling.
			});
	};

}