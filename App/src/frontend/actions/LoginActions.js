import {ipcRenderer} from 'electron';
import http from './../../core/http';
import NotificationActions from './NotyficationActions';
import socketActions from './socketActions';


export default {
	login,
	logout,
	changePassword
};

function login(payload) {
	return dispatch => {
		return http.post('/login',payload)
			.then(({data}) => {
				ipcRenderer.send('LOGIN', data);
				return dispatch({
					type: 'LOGIN',
					payload: {
						id: data.id,
						email: data.email,
						image: data.image,
						username: data.username
					}
				});
			})
			.then(() =>
				dispatch({
					type: '@@router/LOCATION_CHANGE',
					payload: {
						action: 'POP',
						hash: '',
						key: null,
						pathname: '/chat',
						search: ''
					}
				}))
			.catch((e) => {
				dispatch(NotificationActions.notifyError('Wrong username/password'));
		});
	};
}

function logout() {
	return dispatch => {
		return http.post('/logout')
			.then(() => dispatch(socketActions.disconnect()))
			.then(() =>
				dispatch({
					type: 'LOGOUT',
					payload: {
						isAuthenticated: false
					}
				})
			);
	};
}

function changePassword(payload) {
	return dispatch => {
		return http.post('/change-password', payload)
			.then(() => dispatch({
				type: 'PASSWORD_CHANGED',
				payload: {}
			}))
			.then(() => dispatch(
				NotificationActions.notify('Password changed')
			));
	};
}