import Notifications from 'react-notification-system-redux';

export default {
	notifyError,
	notify
};

function notifyError(error) {
	return dispatch => {
		const notificationOtps = {
			title: 'An error has occurred',
			message: error.toString(),
			position: 'br',
			autoDismiss: 10
		};

		dispatch(Notifications.error(notificationOtps));
	};
}


function notify(info) {
	return dispatch => {
		const notificationOtps = {
			title: 'Attention !!!',
			message: info,
			position: 'br',
			autoDismiss: 10
		};

		dispatch(Notifications.info(notificationOtps));
	};
}