import _ from 'lodash';
import http from './../../core/http';
import NotificationActions from './NotyficationActions';

export default  {
	testAction,
	buttonAction,
	modalAction,
	authAction,
	uploadImage,
	imageUploadError,
	userStatusChanged
};

function testAction() {

	http.get('/test/url/for/testing?idOver=9999999')
		.then((response) => {
			console.log(response);
		});

	return {
		type: 'TEST_ACTION',
		payload: {
			text: 'test action'
		}
	};
}

function buttonAction(e) {
	e++;
	return {
		type: 'BTN_ACTION',
		payload: {
			count: e
		}
	};
}

function modalAction(currentModalState) {
	return {
		type: 'MODAL_ACTION',
		payload: {
			modalOpen: !currentModalState
		}
	};
}

function authAction() {
	return {
		type: 'AUTH_ACTION',
		payload: {
			isAuthenticated: true
		}
	};
}

function uploadImage(payload) {
	return (dispatch, getState) => {
		const userInfo = getState().user;

		const httpPayload = {
			id: userInfo.id,
			image: payload
		};

		return http.post('/image', httpPayload)
			.then((data)=> {
				console.log(data);
				return dispatch({
					type: 'IMAGE_UPLOAD',
					payload: {
						image: data.data.image
					}
				});
			})
			.then(() => {
				return dispatch(NotificationActions.notify('Your image is uploading'));
			});
	};
}

function imageUploadError() {
	return (dispatch) => {
		return dispatch(NotificationActions.notifyError('Uploaded image should be JPEG or PNG'));
	};
}

function userStatusChanged(data, status) {
	return (dispatch, getState) => {
		const users = getState().contacts.savedContacts;
		const userIdx = _.findIndex(users, (usr) => usr.id === data.id);
		users[userIdx].status = status;

		dispatch({
			type: 'USER_STATUS',
			payload: users
		});
	};
}

