import _ from 'lodash';
import socketActions from './socketActions';
import { ipcRenderer } from 'electron';

export default {

	messageReceived(payload) {
		return (dispatch, getState) => {

			const selectedUser = getState().application.selectedUser;
			payload.message.trueDate = new Date(payload.date);
			payload.message.date = new Date(payload.date).toDateString();
			payload.selectedUser = selectedUser ? selectedUser.id : '';
			payload.message.sender = getState().user.id == payload.message.sender ? null : payload.message.sender;
			payload.message.message = decodeURIComponent(window.atob(payload.message.message));


			if (payload.message.sender) {
				const contactMap = getState().contacts.savedContactsMap;

				const user = contactMap[payload.message.sender];
				payload.message.name = user.username;
				payload.message.image = user.image;

			}

			dispatch({
				type: 'MESSAGE_RECEIVED',
				payload
			});

			if (payload.message.sender) {
				dispatch(this.sound());
				ipcRenderer.send('NOTIFICATION', { message: payload.message });
			}

		};
	},

	sendMessage(msg) {
		return {
			type: 'MESSAGE_SEND',
			payload: {
				socketConnected: true
			}
		};
	},

	sound() {
		return {
			type: 'MSG',
			payload: {},
			meta: { sound: 'message' }
		};
	},

	messagesLoaded(data) {
		return (dispatch, getState) => {
			const userId = getState().user.id;
			const contactMap = getState().contacts.savedContactsMap;

			const messages = _.map(data.messages, (message) => {
				message.trueDate = new Date(message.date);
				message.date = new Date(message.date).toDateString();
				message.sender = userId == message.sender ? null : message.sender;
				message.message = decodeURIComponent(window.atob(message.message));

				if (message.sender) {
					const user = contactMap[message.sender];
					message.name = user.username;
					message.image = user.image;

				}

				return message;
			});


			dispatch({
				type: 'MESSAGES_LOADED',
				payload: {
					id: data.id,
					messages: _.orderBy(messages, 'trueDate')
				}
			});
		};
	},

	getMessages(contact) {
		return (dispatch, getState) => {
			const messageKey = _.sortBy([getState().user.id, contact.id]);

			dispatch({
				type: 'USER_SELECTED',
				payload: {
					id: messageKey
				}
			});

			const { messages } = getState();
			if (messages.messagesMap[messageKey] == null) {
				socketActions.sendSocket('MESSAGE_REQUEST', { messageKey });
			}
		};
	},

	loadMoreMessages(loadedData) {
		return (dispatch, getState) => {
			const selectedUser = getState().application.selectedUser;
			let payload = loadedData;

			if (selectedUser == loadedData.chatRoomId) {
				payload = _.assign({}, loadedData, { userOpened: true });
			}

			payload.messages = _.map(payload.messages, (msg) => decodeURIComponent(window.atob(msg.message)));

			return dispatch({
				type: 'MESSAGES_OLD_LOADED',
				payload: {
					payload
				}
			});
		};
	}
};
