import settings from 'electron-settings';

export default {
	setTheme(theme) {
		settings.set('userSettings.theme', theme );
		return {
			type: 'SET_THEME',
			payload: {
				theme
			}
		};
	},

	setSound(soundEnabled) {
		settings.set('userSettings.soundEnabled', soundEnabled);
		return {
			type: 'TOGGLE_SOUND',
			payload: {
				soundEnabled
			}
		};
	}
};