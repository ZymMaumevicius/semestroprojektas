import socketActions from './socketActions';
import Notifications from 'react-notification-system-redux';

export default {
	contactsReceived(contacts) {
		console.log(contacts);
		return dispatch => {
			return dispatch({
				type: 'CONTACTS_RECEIVED',
				payload: {
					contactList: contacts
				}
			});
		};
	},

	addContact(contactId) {
		return dispatch => {
			socketActions.sendSocket('CONTACTS_ADD-CONTACT', {contactId, initialRequest: true});

			const notificationOtps = {
				title: 'Contact request send',
				message: 'Wait till your new contact request will be accepted',
				position: 'br',
				autoDismiss: 10
			};

			dispatch(Notifications.info(notificationOtps));

		};
	},

	addedAsContact({user}) {
		return dispatch => {
			const notificationOtps = {
				title: `Contact request received from ${user.userName}`,
				message: 'Add this contact to your list to see messages from him',
				action: {
					label: 'Add him too',
					callback: function() {
						socketActions.sendSocket('CONTACTS_ADD-CONTACT', {contactId: user.id, initialRequest: false});
					}
				},
				position: 'br',
				autoDismiss: 100
			};

			dispatch(Notifications.info(notificationOtps));
		};
	},

	removeContact(contact) {
		return dispatch => {
			socketActions.sendSocket('CONTACTS_REMOVE-CONTACT', {contactId: contact.id});

			const notificationOtps = {
				title: `Contact ${contact.userName} was removed`,
				message: 'You removed contact from contact list, he will not see you in his contacts',
				position: 'br',
				autoDismiss: 100
			};

			dispatch(Notifications.info(notificationOtps));
		};
	}
};