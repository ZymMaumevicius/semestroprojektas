import _ from 'lodash';
import WebSocketWrapper from './../../core/websocket';
import env from './../../../environment.json';
import channelsConfig from './../channels-config';
import SocketConnectionEnum from '../../enums/SocketConnectionEnum';

export default {
	setupSocketConnection,
	socketConnected,
	sendMessage,
	disconnect,
	sendSocket
};

let socket;

function setupSocketConnection(payload) {
	socket = new WebSocketWrapper(env.socketURL,payload);

	_.forEach(channelsConfig, (func, eventName) => {
		socket.on(eventName, func);
	});

	return {
		type: 'SOCKET_CONNECTION_STARTED',
		payload : {
			connectionStatus : SocketConnectionEnum.CONNECTING
		}
	};
}

function socketConnected() {
	return {
		type: 'SOCKET_CONNECTION_CONNECTED',
		payload : {
			connectionStatus : SocketConnectionEnum.CONNECTED
		}
	};
}

function sendMessage(msg, userToSend) {
	const encorer = new TextEncoder('utf-8');
	const user = socket.getUser();
	const obj = {
		date: new Date(),
		message: {
			id: userToSend.id,
			sender: user.id,
			message: window.btoa(encodeURIComponent(msg))
		},
		users: userToSend
	};


	socket.send('MESSAGE_SEND', obj);
	return {
		type: 'MESSAGE_SEND',
		payload: {
			socketConnected: true
		}
	};
}

function disconnect() {
	socket.close();
	return dispatch => {
		dispatch({
			type: '@@router/LOCATION_CHANGE',
			payload: {
				action: 'POP',
				hash: '',
				key: null,
				pathname: '/',
				search: ''
			}
		});
	};
}

function sendSocket(type, data) {
	socket.send(type, data);
}

