import _ from 'lodash';
import env from '../../../environment.json';

export default {
	test
};

const testSocket = (i) => {
	const socket = new WebSocket(env.socketURL + '/ws');
	socket.onopen = () => {
		let obj = {
			eventName: 'SOCKET_CONNECTED',
			user: 'asd' + i
		};
		socket.send(JSON.stringify(obj));

		obj.eventName = 'MESSAGE_SEND';
		obj = _.assign(obj, { data: { message: 'textas', users: ['asdasd'] } });

		for (let v = 0; v < 500; v++) {
			setTimeout(() => {
				console.log(JSON.stringify(obj))
				socket.send(JSON.stringify(obj));
			}, 1000);
		}
	};
};

function test() {
	for (let i = 0; i < 300; i++) {
		testSocket(i);
	}
}