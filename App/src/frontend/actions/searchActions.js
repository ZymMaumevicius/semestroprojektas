import socketActions from './socketActions';
export default {
	filter,
	filterFromServer
};

function filter(payload) {
	return dispatch => {
		dispatch({
			type: 'FILTER',
			payload: {
				filterKeyword: payload
			}
		});

		if (payload.length > 0) {
			socketActions.sendSocket('CONTACTS_SEARCH-CONTACT', { term: payload });
		}
	};
}

function filterFromServer(payload) {
	return {
		type: 'SERVER_CONTACT_RESULT',
		payload: {
			serverContacts: payload
		}
	};
}