import path from 'path';
import soundsMiddleware from 'redux-sounds';

export default soundsMiddleware({
	message : {
		urls: [ path.join(__dirname, '../public/sound/sound.wav' )],
		volume: 0.75
	}
});