export const
	whitespacePattern = /\s/g,
	passwordDigitsPattern = /(?=.*\d)/g,
	passwordLowercasePattern = /(?=.*[a-z])/g,
	passwordUppercasePattern = /(?=.*[A-Z])/g,
	emailPattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
