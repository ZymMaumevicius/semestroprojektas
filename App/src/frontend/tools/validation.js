import * as RegExpressions from './regexp/validationExpressions';

export function validatePassword(password) {
	if (!RegExpressions.passwordDigitsPattern.test(password)) {
		return 'Password must contain digits';
	} else if (!RegExpressions.passwordLowercasePattern.test(password)) {
		return 'Password must lowercase characters';
	} else if (!RegExpressions.passwordUppercasePattern.test(password)) {
		return 'Password must uppercase characters';
	} else if (password.length < 8) {
		return 'Password must be at least 8 characters long';
	} else if (RegExpressions.whitespacePattern.test(password)) {
		return 'Password must not contain whitespaces';
	}
	return '';
}

export function validateEmail(email) {
	if (!RegExpressions.emailPattern.test(email)) {
		return 'Email address is not valid';
	}
	return '';
}

export function validateUsername(nickname) {
	if (nickname.length < 4) {
		return 'Username too short';
	} else if (nickname.length > 16) {
		return 'Username too long';
	} else if (RegExpressions.whitespacePattern.test(nickname)) {
		return 'Username must not contain whitespace characters';
	}
	return '';
}