import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';
import { createHashHistory } from 'history';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';

import Store from './store';
import MainWindow from './views/MainWindow';
import Login from './views/Login';
import Registration from './views/Registration';
import MainWrapper from './views/App/MainWrapper';

const history = syncHistoryWithStore(createHashHistory(), Store);

Store.dispatch({
	type: 'INIT_SETTINGS'
});

ReactDOM.render((
		<Provider store={Store}>
			<Router history={history}>
				<Route path="/" component={MainWrapper}>
					<IndexRoute component={Login}/>
					<Route path="/registration" component={Registration}/>
					<Route path="/chat" component={MainWindow} />
				</Route>
			</Router>
		</Provider>
	),
	document.getElementById('app')
);