import React, { PropTypes } from 'react';
import classNames from 'classnames';

export default function SimpleInput(props) {
	const wrapperClass = classNames({
		'input-single-column': !props.settings});

	const descriptionClass = classNames(
		'input-description',
		{'settings-input-description': props.settings});

	const inputClass = classNames(
			'input-field',
		{'settings-input': props.settings});

	const errorClass = classNames(
		'input-has-error',
		{'settings-input-error': props.settings});

	return (
		<div className={wrapperClass}>
			<label className={descriptionClass}>{props.name}</label>
			<input
				className={inputClass}
				name={props.name}
				type={props.type}
				onChange={props.onChange}
			/>
			<div className={errorClass}>{props.errorMessage}</div>
		</div>
	);
}

SimpleInput.propTypes = {
	errorMessage: PropTypes.string,
	name: PropTypes.string,
	type: PropTypes.string,
	onChange: PropTypes.func
};
