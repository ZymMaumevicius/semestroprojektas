import _ from 'lodash';

const defaultState = {
	savedContacts: [],
	filteredContacts: [],
	savedContactsMap: {}
};
export default function (state = defaultState, action) {
	const { payload, type } = action;
	switch (type) {
		case 'FILTER':
			const filtered = _.filter(state.savedContacts, (contact) => {
				return contact.username.toLowerCase().includes(payload.filterKeyword);
			});
			return _.assign({}, state, { filteredContacts: filtered });
		case 'CONTACTS_RECEIVED':
			const newSavedContactsMap = _.keyBy(payload.contactList, 'id');
			_.forEach(newSavedContactsMap, (contact) => {
				contact.image = contact.image || 'http://placehold.it/64x64';
			});

			return _.assign({}, state, { savedContacts: payload.contactList, savedContactsMap: newSavedContactsMap });
		case 'SERVER_CONTACT_RESULT':
			return _.assign({}, state, { serverContacts: payload.serverContacts });
		case 'USER_STATUS':
			const map = _.keyBy(payload, 'id');
			return _.assign({}, state, {savedContacts: payload, savedContactsMap: map});
		case 'LOGOUT':
			return defaultState;
		default:
			return state;
	}
}