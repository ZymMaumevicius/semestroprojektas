import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as notifications } from 'react-notification-system-redux';

import appStateReducer from './appStateReducer';
import userReducer from './userReducer';
import contactsReducer from './contactsReducer';
import messageReducer from './messageReducer';
import settingsReducer from './settingsReducer';

export default combineReducers({
	application: appStateReducer,
	user: userReducer,
	contacts: contactsReducer,
	routing: routerReducer,
	messages: messageReducer,
	settings: settingsReducer,
	notifications
});