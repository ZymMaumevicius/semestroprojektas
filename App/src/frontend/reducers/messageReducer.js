import _ from 'lodash';

const defaultState = {
	messagesMap: {},
	messages: []
};

export default function (state = defaultState, action) {
	const { type, payload } = action;
	switch (type) {
		case 'MESSAGES_LOADED':
			const newMessageMap = _.clone(state.messagesMap);
			newMessageMap[_.sortBy(payload.id)] = payload.messages;

			return _.assign({}, state, { messagesMap: newMessageMap, messages: payload.messages });
		case 'USER_SELECTED':
			const userMessage = state.messagesMap[payload.id] || [];

			return _.assign({}, state, { messages: userMessage });
		case 'MESSAGE_RECEIVED':
			const key = _.sortBy(payload.message.id);
			const newMessages = _.clone(state.messagesMap);
			if (!newMessages[key]) {
				newMessages[key] = [];
			}

			newMessages[key].push(payload.message);

			if (payload.selectedUser.toString() === key.toString()) {
				const msgs = _.clone(newMessages[key]);

				return _.assign({}, state, { messagesMap: newMessages, messages: msgs });
			}

			return _.assign({}, state, { messagesMap: newMessages });
		case 'OLDER_MESSAGES_LOADED':
			const newState = _.clone(state.messagesMap);
			const messages = _.clone(state.messages);
			newState[payload.chatRoomId].unshift(payload.messages);

			if (payload.userOpened) {
				messages.unshift(payload.messages);
			}

			return _.assign({}, state, {newMessageMap: newState, messages: messages});
		case 'LOGOUT' :
			return defaultState;
		default:
			return state;
	}
}