import _ from 'lodash';

export default function (state = {
	isAuthenticated: false,
	socketConnected: false,
	modalOpen: false
}, action) {
	const { payload, type } = action;
	switch (type) {
		case 'LOGIN':
			return _.assign({}, payload);
		case 'TEST_ACTION':
			return _.assign({}, state, { text: payload.text });
		case 'IMAGE_UPLOAD' :
			return _.assign({}, state, { image: payload.image });
		case 'MODAL_ACTION' :
			return _.assign({}, state, { modalOpen: payload.modalOpen });
		case 'PASSWORD_CHANGED':
			return _.assign({}, state);
		case 'LOGOUT' :
			return {};
		default:
			return state;
	}
}
