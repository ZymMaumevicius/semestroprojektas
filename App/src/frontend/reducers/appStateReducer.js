import _ from 'lodash';

const defaultState = {
	selectedUser: null
};

export default function (state = defaultState, action) {
	const { type, payload } = action;
	switch (type) {
		case 'USER_SELECTED':
			return _.assign({}, state, { selectedUser: payload });
		case 'LOGOUT':
			return defaultState;
		default:
			return state;
	}
}