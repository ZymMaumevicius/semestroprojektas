import _ from 'lodash';
import settings from 'electron-settings';

export default function (state = {}, action) {
	const { payload, type } = action;
	switch (type) {
		case 'INIT_SETTINGS':
			const userSettings = settings.get('userSettings');
			return _.assign({}, state, { theme: userSettings.theme, soundEnabled: userSettings.soundEnabled });
		case 'SET_THEME':
			return _.assign({}, state, { theme: payload.theme });
		case 'TOGGLE_SOUND':
			return _.assign({}, state, { soundEnabled: payload.soundEnabled });
		default:
			return state;
	}
}
