import React, { PropTypes } from 'react';
import classnames from 'classnames';

export default function ContactCard({ username, status, image, onClick, onAdd, added }) {
	const indicatorClass = classnames('indicator', {
		'green': status,
		'white' : !status
	});
	const img = image ? image : 'http://placehold.it/64x64';

	return (
		<div className="contact-wrapper" onClick={added && onClick}>
			<img className="contact-image" src={img}/>
			<span className="contact-name">	{username}</span>
			<div className="indicator-wrapper">
				<div className={indicatorClass} title={status}/>
			</div>
			{!added && (
				<span className="contact-add" onClick={onAdd}/>
			)}
		</div>
	);
}

ContactCard.propTypes = {
	username: PropTypes.string.isRequired,
	status: PropTypes.string.isRequired,
	image: PropTypes.string.isRequired,
	onClick: PropTypes.func,
	onAdd: PropTypes.func,
	added: PropTypes.bool.isRequired
};