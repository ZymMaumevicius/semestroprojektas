import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import ContactCard from './ContactCard';
import contactActions from '../../../actions/contactsActions';
import messageActions from '../../../actions/messageActions';
import { ContextMenu, MenuItem, ContextMenuTrigger } from 'react-contextmenu';


@connect((store) =>{
	return {
		contactsInitStuff: store.contacts
	};
})
export default class ContactList extends Component {
	constructor(args) {
		super(args);
	}

	render() {
		return (
			<div className="contact-list-wrapper">
				{this.renderContacts()}
				{this.props.serverContacts &&
				<hr className="contacts-divider"/>}
				{this.renderServerContacts()}
			</div>
		);
	}

	renderContacts() {
		return _.map(this.props.contacts, (contact) => {
			return (
				<div>
					<ContextMenuTrigger id={contact.id}>
						<ContactCard
							key={contact.id}
							onClick={() => this.handleContactClick(contact)}
							image={contact.image}
							username={contact.username}
							status={contact.status}
							added={true}/>
					</ContextMenuTrigger>

					<ContextMenu id={contact.id}>
						<MenuItem onClick={() => this.removeContact(contact)}>
							Remove user
						</MenuItem>
					</ContextMenu>
				</div>
			);
		});
	}

	renderServerContacts() {
		if (this.props.serverContacts.length !== 0) {
			return _.map(this.props.serverContacts, (contact) => {
				return (<ContactCard
						username={contact.username}
						image={contact.image}
						key={contact.id}
						status={'unknown'}
						onAdd={this.handleContactAdd.bind(this, contact)}
						added={false}/>
				);
			});
		}
		return (
			<div className="contacts-error">No contacts found</div>
		);
	}

	handleContactClick(contact) {
		this.props.dispatch(messageActions.getMessages(contact));
	}

	handleContactAdd(contact) {
		this.props.dispatch(contactActions.addContact(contact.id));
	}

	removeContact(contact) {
		this.props.dispatch(contactActions.removeContact(contact));
	}
}