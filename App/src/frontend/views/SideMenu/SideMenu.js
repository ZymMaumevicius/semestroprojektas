import React from 'react';
import { connect } from 'react-redux';

import ContactList from './Contacts/ContactList';
import UserAndSearch from './UserAndSearch';
import SearchActions from '../../actions/searchActions';


@connect((store) => {
	return {
		contacts: store.contacts,
		user: store.user
	};
})
export default class SideMenu extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			filterKeyword: ''
		};
	}

	render() {
		return (
			<div className="side-menu-wrapper">
				<UserAndSearch
					image={this.props.user.image}
					name={this.props.user.username}
					onSearchInput={this.onSearchInput.bind(this)}
				/>
				<ContactList
					contacts={this.state.filterKeyword ? this.props.contacts.filteredContacts : this.props.contacts.savedContacts}
					serverContacts={!!this.state.filterKeyword && this.props.contacts.serverContacts}
				/>
			</div>
		);
	}

	onSearchInput(keyword) {
		this.setState({filterKeyword: keyword});
		this.props.dispatch(SearchActions.filter(keyword.toLowerCase()));
	}
}