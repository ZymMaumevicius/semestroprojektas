import React from 'react';

export default class UserAndSearch extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			searchContact: ''
		};
	}

	render() {
		const image = (this.props.image && this.props.image.length > 0 ) ? this.props.image : 'http://placehold.it/64x64';

		return (
			<div className="user-info-wrapper">
				<div className="user-info">
					<img className="contact-image" src={image}/>
					<span className="user-name">		{this.props.name}</span>
				</div>
				<div className="contact-search">
					<span className="search-icon"/>
					<input
						className="search-input"
						placeholder="Search contacts"
						onChange={this.onChange.bind(this)}
					/>
				</div>
			</div>
		);
	}

	onChange(e) {
		this.props.onSearchInput(e.target.value);
	}
}