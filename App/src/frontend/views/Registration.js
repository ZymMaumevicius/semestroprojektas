import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import autobind from 'autobind-decorator';

import SimpleInput from '../components/simpleInput';
import { validateEmail, validatePassword, validateUsername } from '../tools/validation';
import RegistrationActions from '../actions/RegisterActions';

@connect((store) => {
	return {
		routing: store.routing
	};
})
@autobind
export default class Registration extends Component {
	constructor(props) {
		super(props);
		this.state = {
			validNickname: '',
			validPassword: '',
			validEmail: '',
			userName: '',
			passwordError: '',
			passwordReenterError: '',
			emailError: ''
		};
	}

	render() {
		return (
			<div className="logreg-container">
				<form onSubmit={this.handleSubmit}>
					<SimpleInput
						name="Enter your email address"
						type="text"
						errorMessage={this.state.emailError}
						onChange={this.handleChange.bind(this, 'email')}
					/>
					<SimpleInput
						name="Enter username"
						type="text"
						errorMessage={this.state.userName}
						onChange={this.handleChange.bind(this, 'username')}/>
					<SimpleInput
						name="Enter password"
						type="password"
						errorMessage={this.state.passwordError}
						onChange={this.handleChange.bind(this, 'password')}/>
					<SimpleInput
						name="Re-enter password"
						type="password"
						errorMessage={this.state.passwordReenterError}
						onChange={this.handleChange.bind(this, 'password2')}/>
					<button className="button-logreg" type="submit">Submit</button>
					<Link to="/" className="button-logreg">Go back</Link>
				</form>
			</div>
		);
	}

	handleChange(name, event) {
		switch (name) {
			case 'username': {
				const error = validateUsername(event.target.value);
				this.setState({
					userName: error,
					validNickname: error ? '' : event.target.value
				});
				break;
			}
			case 'password': {
				const error = validatePassword(event.target.value);
				this.setState({
					passwordError: error,
					firstInputPassword: error ? '' : event.target.value
				});
				break;
			}
			case 'password2': {
				this.setState({ validPassword: '' });
				this.matchPassword(event.target.value);
				break;
			}
			case 'email': {
				this.setState({ validEmail: '' });
				const error = validateEmail(event.target.value);
				this.setState({
					emailError: error,
					validEmail: error ? '' : event.target.value
				});
				break;
			}
		}
	}

	matchPassword(secondInputPassword) {
		if (this.state.firstInputPassword !== secondInputPassword) {
			this.setState({ passwordReenterError: 'Passwords do not match' });
		} else {
			this.setState({
				passwordReenterError: '',
				validPassword: this.state.firstInputPassword
			});
		}
	}

	handleSubmit(e) {
		e.preventDefault();
		if (this.state.validNickname && this.state.validPassword && this.state.validEmail) {
			const payload = {
				userName: this.state.validNickname,
				password: this.state.validPassword,
				email: this.state.validEmail
			};

			this.props.dispatch(RegistrationActions.register(payload));
		} else {
			if (!this.state.validUsername) {
				this.setState({ userName: 'No valid username entered' });
			}
			if (!this.state.validPassword) {
				this.setState({ passwordError: 'No valid password entered' });
			}
			if (!this.state.validEmail) {
				this.setState({ emailError: 'No valid email entered' });
			}
		}
	}
}
