import React from 'react';
import classnames from 'classnames';

export default function Message({ message }) {
	const messageClass = classnames('msg',
		{ 'self': !message.sender },
		{ 'other': message.sender }
	);

	return (
		<li className={messageClass}>
			<div className="msg-container">
				{message.sender && (<img className="msg-photo" src={message.image} title={message.name}/>)}
				<div className="msg-text-container">
					<span>{message.name}</span><span className="msg-date">{message.date}</span>
					<div className="msg-text">
						{message.message}
					</div>
				</div>

			</div>
		</li>
	);
}