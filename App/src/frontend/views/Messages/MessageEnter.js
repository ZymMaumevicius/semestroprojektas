import React, { Component } from 'react';
import keydown from 'react-keydown';

export default class MessageEnter extends Component {
	constructor(props) {
		super(props);
		this.state = { message: '' };
	}

	render() {
		return (
			<div className="message-input-wrapper">
				<textarea
					className="message-input"
					placeholder="Enter your text here..."
					onInput={this.handleInput.bind(this)}
					value={this.state.message}
					onKeyDown={this.handleSend.bind(this)}
				>Enter your text here...</textarea>
			</div>
		);
	}

	handleInput(e) {
		this.setState({
			message: e.target.value
		});
	}

	@keydown( 'enter' )
	handleSend(e) {
		e.preventDefault();
		if (this.state.message.trim().length > 0) {
			this.props.onMessageSend(this.state.message.trim());
			this.setState({ message: '' });
		}
	}
}
