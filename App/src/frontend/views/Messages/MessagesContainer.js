import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { findDOMNode } from 'react-dom';
import autobind from 'autobind-decorator';
import MessageEnter from './MessageEnter';
import Messages from './Messages';
import socketActions from '../../actions/socketActions';

@connect((store) => {
	return {
		message: store.messages,
		contacts: store.contacts,
		routing: store.routing,
		application: store.application
	};
})
@autobind
export default class MessagesContainer extends Component {
	constructor(props) {
		super(props);
		this.scrollToBottom = this.scrollToBottom.bind(this);
	}

	scrollToBottom() {
		const messagesContainer = findDOMNode(this.messagesContainer);
		messagesContainer.scrollTop = messagesContainer.scrollHeight;
	}

	componentDidMount() {
		this.scrollToBottom();
	}

	componentDidUpdate() {
		this.scrollToBottom();
	}

	render() {
		const messages = this.props.message.messages;

		return (
			<div className="message-wrapper">
				<Messages messages={messages}
						  loadOlderMessages={this.loadOlderMessages}
						  ref={(el) => {
							  this.messagesContainer = el;
						  }}/>
				<MessageEnter onMessageSend={this.handleMessageSend}/>
			</div>
		);
	}

	handleMessageSend(message) {
		this.props.dispatch(socketActions.sendMessage(message, this.props.application.selectedUser));
	}

	loadOlderMessages(length) {
		socketActions.sendSocket('MESSAGE_LOAD_MORE', {
			length,
			chatRoomId: _.orderBy(this.props.application.selectedUser).toString()
		});
	}
}