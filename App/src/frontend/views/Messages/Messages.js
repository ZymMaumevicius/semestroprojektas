import _ from 'lodash';
import React, { Component } from 'react';

import Message from './Message';

export default class Messages extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const messages = _.map(this.props.messages, (message, index) => (<Message key={index} message={message}/>));

		return (
			<ol className="chat">
				<li>
					<span className="msg-link"  onClick={()=> this.props.loadOlderMessages(messages.length)}> Load older messages</span>
				</li>
				{messages}
			</ol>
		);
	}

}