import React, { Component } from 'react';
import autobind from 'autobind-decorator';
import { connect } from 'react-redux';

import SettingsActions from '../../actions/settingsActions';

@connect()
@autobind
export default class SoundSettings extends Component {
	constructor(props) {
		super(props);

		this.state = {
			soundEnabled: this.props.soundEnabled
		};
	}

	render() {
		return (
			<div className="settings-container">
				<form onSubmit={this.handleSubmit}>
					<div>
						<input type="checkbox" value={this.state.soundEnabled} className="settings-checkbox" onChange={this.handleInputClick} checked={this.state.soundEnabled}/>
						<span className="settings-checkbox-title">Enable sounds</span>
					</div>
					<button className="button-logreg settings-btn" type="submit">Save</button>
				</form>
				<br/>
				<span className="settings-checkbox-title">Note: changes will appear only after restart of application</span>
			</div>
		);
	}

	handleInputClick(e) {
		this.setState({
			soundEnabled: e.target.checked
		});
	}

	handleSubmit(e) {
		e.preventDefault();
		this.props.dispatch(SettingsActions.setSound(this.state.soundEnabled));
	}
}