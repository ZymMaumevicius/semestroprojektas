import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import autobind from 'autobind-decorator';
import Dropzone from 'react-dropzone';

import UserActions from '../../actions/userActions';

@connect()
@autobind
export default class ImageSettings extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="settings-container">
				<Dropzone className="dropzone-container" onDrop={this.onDrop}>
					<p>Drop your picture here</p>
				</Dropzone>
			</div>
		);
	}

	onDrop(e) {
		const imgFormat = ['image/jpeg', 'image/png'];
		if (e.length > 1 || !_.some(imgFormat, (p) => p === e[0].type)) {
			return this.props.dispatch(UserActions.imageUploadError());
		}

		const reader = new FileReader();

		reader.onloadend = () => {
			this.props.dispatch(UserActions.uploadImage(reader.result));
		};

		reader.readAsDataURL(e[0]);
	}
}