import React, { Component } from 'react';
import autobind from 'autobind-decorator';
import { connect } from 'react-redux';

import { validatePassword } from '../../tools/validation';
import SimpleInput from '../../components/simpleInput';
import LoginAction from '../../actions/LoginActions';

@connect((store) => {
	return {
		user: store.user
	};
})
@autobind
export default class PasswordSettings extends Component {
	constructor(props) {
		super(props);
		this.state = {
			oldPassword: '',
			validPassword: '',
			passwordError: '',
			passwordReenterError: ''
		};
	}

	componentWillReceiveProps() {
		this.setState({
			oldPassword: '',
			validPassword: '',
			passwordError: '',
			passwordReenterError: ''
		});
	}

	render() {
		return (
			<div className="settings-container">
				<form onSubmit={this.handleSubmit}>
					<SimpleInput
						name="Current password:"
						type="password"
						settings={true}
						onChange={this.handleChange.bind(this, 'oldPassword')}/>
					<SimpleInput
						name="New password:"
						type="password"
						settings={true}
						errorMessage={this.state.passwordError}
						onChange={this.handleChange.bind(this, 'newPassword')}/>
					<SimpleInput
						name="Repeat new password:"
						type="password"
						settings={true}
						errorMessage={this.state.passwordReenterError}
						onChange={this.handleChange.bind(this, 'confirmPassword')}/>
					<button className="button-logreg settings-btn" type="submit">Save
					</button>
				</form>
			</div>
		);
	}

	handleChange(name, event) {
		switch (name) {
			case 'oldPassword':
				this.setState({ oldPassword: event.target.value });
				break;
			case 'newPassword':
				const error = validatePassword(event.target.value);
				this.setState({
					passwordError: error,
					firstInputPassword: error ? '' : event.target.value
				});
				break;
			case 'confirmPassword':
				this.setState({ validPassword: '' });
				this.matchPassword(event.target.value);
				break;
		}
	}

	matchPassword(secondInputPassword) {
		if (this.state.firstInputPassword !== secondInputPassword) {
			this.setState({ passwordReenterError: 'Passwords do not match' });
		} else {
			this.setState({
				passwordReenterError: '',
				validPassword: this.state.firstInputPassword,
				id: this.props.user.id
			});
		}
	}

	handleSubmit(e) {
		e.preventDefault();
		if (this.state.oldPassword && this.state.validPassword) {
			this.props.dispatch(LoginAction.changePassword({
				newPassword: this.state.validPassword,
				oldPassword: this.state.oldPassword,
				id: this.props.user.id
			}));
		} else {
			if (!this.state.validPassword) {
				this.setState({ passwordError: 'No valid password entered' });
			}
		}
	}
}