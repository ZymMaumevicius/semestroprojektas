import React, { Component } from 'react';
import autobind from 'autobind-decorator';
import Dropdown from 'react-dropdown';
import { connect } from 'react-redux';

import SettingsActions from '../../actions/settingsActions';

@connect()
@autobind
export default class ThemeSettings extends Component {
	constructor(props) {
		super(props);

		this.state = {
			options: ['dark', 'light'],
			currentTheme: this.props.currentTheme
		};
	}

	render() {
		return (
			<div className="settings-container">
				<form onSubmit={this.handleSubmit}>
					<label className="input-description settings-input-description">Theme:</label>
					<Dropdown options={this.state.options} value={this.state.currentTheme} onChange={this.handleDropdown}/>
					<button className="button-logreg settings-btn" type="submit">Save</button>
				</form>
			</div>
		);
	}

	handleDropdown(e) {
		this.setState({
			currentTheme: e.value
		});

	}

	handleSubmit(e) {
		e.preventDefault();
		this.props.dispatch(SettingsActions.setTheme(this.state.currentTheme));
	}
}