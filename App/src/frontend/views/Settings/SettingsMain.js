import React, { Component, PropTypes } from 'react';
import Collapse from 'react-collapse';
import autobind from 'autobind-decorator';
import { connect } from 'react-redux';

import PasswordSettings from './PasswordSettings';
import SoundSettings from './SoundSettings';
import ThemeSettings from './ThemeSettings';
import ImageSettings from './ImageSettings';

@connect((store) => {
	return {
		userSettings : store.settings
	};
})
@autobind
export default class SettingsMain extends Component {
	constructor(props) {
		super(props);
		this.state = {
			credentialsOpen: false,
			soundsOpen: false,
			themeOpen: false,
			imageOpen: false
		};
	}

	render() {
		if (this.props.openModal) {
			return (
				<div className="settings">
					<div onClick={this.handleCollapser.bind(this, 'credentialsOpen')} className="settings-collapser-title">
						Change password
						{this.state.credentialsOpen &&
						<span className="settings-collapser-off"/>}
						{!this.state.credentialsOpen &&
						<span className="settings-collapser-on"/>}
					</div>
					<Collapse isOpened={this.state.credentialsOpen}>
						<PasswordSettings/>
					</Collapse>
					<div onClick={this.handleCollapser.bind(this, 'soundsOpen')} className="settings-collapser-title">
						Sound settings
						{this.state.soundsOpen &&
						<span className="settings-collapser-off"/>}
						{!this.state.soundsOpen &&
						<span className="settings-collapser-on"/>}
					</div>
					<Collapse isOpened={this.state.soundsOpen}>
						<SoundSettings
							soundEnabled={this.props.userSettings.soundEnabled}
						/>
					</Collapse>
					<div onClick={this.handleCollapser.bind(this, 'themeOpen')} className="settings-collapser-title">
						Visual settings
						{this.state.themeOpen &&
						<span className="settings-collapser-off"/>}
						{!this.state.themeOpen &&
						<span className="settings-collapser-on"/>}
					</div>
					<Collapse isOpened={this.state.themeOpen}>
						<ThemeSettings
							currentTheme={this.props.userSettings.theme}
						/>
					</Collapse>
					<div onClick={this.handleCollapser.bind(this, 'imageOpen')} className="settings-collapser-title">
						Image upload
						{this.state.imageOpen &&
						<span className="settings-collapser-off"/>}
						{!this.state.imageOpen &&
						<span className="settings-collapser-on"/>}
					</div>
					<Collapse isOpened={this.state.imageOpen}>
						<ImageSettings />
					</Collapse>
				</div>
			);
		}
		return false;
	}

	handleCollapser(collapserTarget) {
		const newState = {};
		newState[collapserTarget] = !this.state[collapserTarget];
		this.setState(newState);
	}


}

SettingsMain.propTypes = {
	openModal: PropTypes.bool.isRequired
};