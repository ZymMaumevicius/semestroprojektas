import _ from 'lodash';
import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import autobind from 'autobind-decorator';
import LoginActions from '../actions/LoginActions';
import userActions from '../actions/userActions';

import SimpleInput from '../components/simpleInput';


@connect((store) => {
	return {
		user: store.user,
		routing: store.routing
	};
})
@autobind
export default class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			username: '',
			password: '',
			usernameError: '',
			passwordError: ''
		};
	}

	render() {
		return (
			<div className="logreg-container">
				<span className="login-logo"/>
				<form onSubmit={this.handleLogin}>
					<SimpleInput
						name="Enter your username"
						type="text"
						errorMessage={this.state.usernameError}
						onChange={this.handleChange.bind(this, 'username')}/>
					<SimpleInput
						name="Enter password"
						type="password"
						errorMessage={this.state.passwordError}
						onChange={this.handleChange.bind(this, 'password')}
					/>
					<button type="submit" className="button-logreg">
						Login
					</button>
					<Link to="/registration" className="button-logreg">Register</Link>
				</form>
			</div>
		);
	}


	handleChange(name, event) {
		switch (name) {
			case 'username': {
				this.setState({ validEmail: '' });
				this.setState({
					username: event.target.value
				});
				break;
			}
			case 'password': {
				this.setState({ password: event.target.value });
				break;
			}
		}
	}

	handleLogin(e) {
		e.preventDefault();
		if (this.state.username && this.state.password) {
			this.setState({ usernameError: '' });
			this.setState({ passwordError: '' });
		} else {
			if (!this.state.username) {
				this.setState({ usernameError: 'No username entered' });
			}
			if (!this.state.password) {
				this.setState({ passwordError: 'No password entered' });
			}
		}

		const payload = _.pick(this.state, ['username', 'password']);

		this.props.dispatch(LoginActions.login(payload));
	}

	setToAuthenticated() {
		this.props.dispatch(userActions.authAction());
	}
}