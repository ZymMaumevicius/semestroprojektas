import React, { Component } from 'react';
import { ipcRenderer } from 'electron';

export default class Header extends Component {
	constructor(props) {
		super(props);

		this.handleLogout = this.handleLogout.bind(this);
		this.handleMinimize = this.handleMinimize.bind(this);
		this.handleMaximize = this.handleMaximize.bind(this);
		this.handleExit = this.handleExit.bind(this);
		this.handleStatusChangeClick = this.handleStatusChangeClick.bind(this);
	}

	render() {
		return (
			<div className="header-wrapper">
				{this.props.isAuthenticated &&
					<div>
						<span className="logout-button" onClick={this.handleLogout} alt="Logout"/>
						<span className="settings-button" onClick={this.props.onSettingsClick}/>
					</div>
				}
				<div className="header-name">
					<span className="header-letter">I</span>
					<span className="header-letter header-letter-intend">S</span>
					<span className="header-logo" onClick={this.handleStatusChangeClick}/>
					<span className="header-letter">N</span>
				</div>
				<span className="minimize-button" onClick={this.handleMinimize}/>
				<span className="maximize-button" onClick={this.handleMaximize}/>
				<span className="exit-button" onClick={this.handleExit}/>
			</div>
		);
	}

	handleLogout() {
		this.props.handleLogout();
	}

	handleStatusChangeClick() {
		this.props.handleStatusChange();
	}

	handleMaximize() {
		ipcRenderer.send('CHANGE_SIZE');
	}

	handleMinimize() {
		ipcRenderer.send('MINIMIZE');
	}

	handleExit() {
		ipcRenderer.send('EXIT');
	}
}