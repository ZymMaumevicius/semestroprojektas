import React, { Component } from 'react';
import { connect } from 'react-redux';
import socketActions from '../actions/socketActions';
import ContactList from './SideMenu/Contacts/ContactList';
import MessageContainer from './Messages/MessagesContainer';
import Settings from './Settings/SettingsMain';
import SideMenu from './SideMenu/SideMenu';


@connect((store) => {
	return {
		user: store.user,
		routing: store.routing,
		application: store.application
	};
})
export default class MainWindow extends Component {
	componentDidMount() {
		this.props.dispatch(socketActions.setupSocketConnection(this.props.user));
	}

	render() {
		return (
			<div>
				<div>
					{!this.props.application.selectedUser
						? <div className="message-wrapper"/>
						: <MessageContainer />
					}
					<SideMenu/>
					<Settings openModal={this.props.user.modalOpen}/>
				</div>
			</div>
		);
	}
}