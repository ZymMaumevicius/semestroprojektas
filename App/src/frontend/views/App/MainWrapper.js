import React, { Component } from 'react';
import { connect } from 'react-redux';
import Notifications from 'react-notification-system-redux';
import settings from 'electron-settings';

import userActions from '../../actions/userActions';
import LoginActions from '../../actions/LoginActions';
import Header from  '../Header/Header';
import socketActions from '../../actions/socketActions';

@connect((store) => {
	return {
		notifications: store.notifications,
		user: store.user,
		settings: store.settings
	};
})
export default class MainWrapper extends Component {
	constructor(...args) {
		super(...args);
	}

	render() {
		function _chooseStyleSheet(theme) {
			const themeLocation = `../public/css/${theme}.css`;
			return (
				<link rel="stylesheet" href={themeLocation} />
			);
		}
		const style = {
			NotificationItem: {
				DefaultStyle: {
					margin: '10px 5px 2px 1px',
					backgroundColor: '#1F1F1F'
				},

				success: {
					color: 'red'
				}
			},

			MessageWrapper: {
				DefaultStyle: {
					color: 'white'
				}
			},

			Dismiss: {
				DefaultStyle: {
					backgroundColor: '#1F1F1F'
				}
			}
		};


		return (
			<div>
				{_chooseStyleSheet(this.props.settings.theme)}
				<Header
					onSettingsClick={this.handleSettingsClick.bind(this)}
					isAuthenticated={!!this.props.user.id}
					handleLogout={this.handleLogout.bind(this)}
				/>
				<Notifications
					notifications={this.props.notifications}
					style={style}
				/>
				{this.props.children}
			</div>
		);
	}

	handleSettingsClick() {
		this.props.dispatch(userActions.modalAction(this.props.user.modalOpen));
	}

	handleLogout() {
		socketActions.disconnect();
		this.props.dispatch(LoginActions.logout());
	}
}