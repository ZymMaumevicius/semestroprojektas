import { applyMiddleware, createStore } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import soundMiddleware from './soundsMiddleware';
import settings from 'electron-settings';

let middleWare;
if (settings.get('userSettings.soundEnabled')) {
	middleWare = applyMiddleware(promise(), thunk, logger(), soundMiddleware);
} else {
	middleWare = applyMiddleware(promise(), thunk, logger());
}

import reducers from './reducers';

export default createStore(reducers, middleWare);