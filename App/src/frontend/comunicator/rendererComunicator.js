import _ from 'lodash';
import { ipcRenderer } from 'electron';

import channels from '../channels-config';

const rendererCommunicator = {
	startCommunication: (user) => {
		rendererCommunicator._sendRequest('SETUP_CONNECTION',user);
		_.forEach(channels, (item, key)=>  {
			rendererCommunicator._listenToChannel(key,item);
		});
	},

	send: (channel, data) => {
		ipcRenderer.send(channel,data);
	},

	_sendRequest: (channel, data) => {
		ipcRenderer.send(channel, data);
	},
	_listenToChannel: (channel, callback) => {
		ipcRenderer.on(channel, callback);
	}
};

export default rendererCommunicator;