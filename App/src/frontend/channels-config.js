import messageActions from './actions/messageActions';
import socketActions from './actions/socketActions';
import notificationActions from './actions/NotyficationActions';
import contactsActions from './actions/contactsActions';
import searchActions from './actions/searchActions';
import userAction from './actions/userActions';
import store from './store';

const channels = {
	'ERROR': (error) => store.dispatch(notificationActions.notifyError(error)),
	'MESSAGE_SENT': (data) => console.log(data.data),
	'MESSAGE_RECEIVED': (event) => store.dispatch(messageActions.messageReceived(event)),
	'MESSAGES_LOADED': (data) => store.dispatch(messageActions.messagesLoaded(data)),
	'BACKEND_STARTED': () => console.log('Backend service started'),
	'SOCKET_CONNECTED': () => store.dispatch(socketActions.socketConnected()),
	'SOCKET_CONNECTED_RESULT': (data) => store.dispatch(contactsActions.contactsReceived(data)),
	'CONTACTS_SEARCH_RESULT': (data) => store.dispatch(searchActions.filterFromServer(data)),
	'CONTACT_ADD_REQUEST': (data) => store.dispatch(contactsActions.addedAsContact(data)),
	'CONTACT_SUCCESSFULLY_ADDED': ({ contacts }) => store.dispatch(contactsActions.contactsReceived(contacts)),
	'OLDER_MESSAGES_LOADED': (data) => store.dispatch(messageActions.loadMoreMessages(data)),
	'USER_CONNECTED': (data) => store.dispatch(userAction.userStatusChanged(data, true)),
	'USER_DISCONNECTED': (data) => store.dispatch(userAction.userStatusChanged(data, false))
};

export default channels;