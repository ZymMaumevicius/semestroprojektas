'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const babel = require('gulp-babel');
const electron = require('gulp-electron-packager');
const exec = require('gulp-exec');
const del = require('del');


const themeFiles = [
	'./src/frontend/styles/dark.scss',
	'./src/frontend/styles/light.scss'];

gulp.task('compile', ['clear', 'sass', 'babelify', 'copyHtmls', 'copyPublic', 'copyEnv'], function (cb) {
	return cb();
});

gulp.task('clear', function (cb) {
	del('dist')
		.then(cb());
});

gulp.task('sass', ['clear', 'babelify', 'copyHtmls'], function (cb) {
	gulp.src(themeFiles)
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./src/public/css'));

	return cb();
});

gulp.task('sass:watch', function () {
	gulp.watch('./src/frontend/styles/*.scss', ['sass']);
});

gulp.task('babelify', ['clear'], function (cb) {
	gulp.src(['./**/*.js', '!./node_modules/**/*.*', '!./dist/**/*.*', '!./gulpfile.js'])
		.pipe(
			babel({
					presets: ['es2016-node5', 'react', 'babili'],
					plugins: [
						['transform-class-properties'],
						['module-resolver', {
							alias: { src: './src' }
						}],
						['transform-decorators-legacy']
					]
				}
			)
		)
		.pipe(gulp.dest('dist'));

	return cb();
});

gulp.task('copyHtmls', ['clear'], function (cb) {
	gulp.src(['./**/*.html', '!./node_modules/**/*.*', '!./dist/**/*.*'])
		.pipe(gulp.dest('dist'));

	return cb();
});

gulp.task('copyPublic', ['clear', 'sass'], function (cb) {
	gulp.src('./src/public/**/*.*')
		.pipe(gulp.dest('./dist/src/public/'));

	return cb();
});

gulp.task('copyEnv', function () {
	return gulp.src('./environment.json')
		.pipe(gulp.dest('dist'));
});

gulp.task('asar', ['compile'], function (cb) {
	del('app.asar')
		.then(() => {
			gulp.src('')
				.pipe(exec('asar pack dist/ app.asar'));

			return cb();
		});
});

gulp.task('package-binaries', ['asar'], function () {
	return gulp.src('')
		.pipe(electron({
			dir: __dirname,
			all: true
		}))
		.pipe(gulp.dest(''));
});

gulp.task('package', ['package-binaries']);

