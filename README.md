# README #

## Main commands in project ##
### App ###
npm start - to run electron
npm run lint - to run ESLint.

### Server ###
in root server directory /Server/Server/Server

dotnet restore - to regenerate modules and project.lock.json

dotnet ef database update - to run migration on database

dotnet migration add {YOUR MIGRATION NAME} - to add migration script.


### App ###

Connection strings are saved in environment.json


## Dependencies ##
### App ###

 *  Electron.js         http://electron.atom.io/
 *  React.js            https://facebook.github.io/react/docs/hello-world.html
 *  Redux               http://redux.js.org/
 *  Socket.IO-client    https://github.com/socketio/socket.io-client
 *  Lodash              https://lodash.com
 *  Knex.js             http://knexjs.org/

### Server ###

 * Entity framework 7 with identity extension
 * NpgSql library in order to connect postgreSql with EF7
 *